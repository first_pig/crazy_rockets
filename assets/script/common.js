const md5 = require('md5');

const common = {
    //域名
    hostConfig : {
        host : 'https://tper.jungongfang.cn/api/',
        appkey : 'Ox8Ojd6lqnEw1UAwFpxf4ayT3PvPHM3t',
        appsecret : 'jvA5Vt1acVyml5Jw7rkIdqm2rKXwcgHK'
    },

    // 用户信息
    userInfo : {},

    //当前最高分数
    maxFenShu : 0,

    //背景音乐配置信息
    bgAudioConfig : {
        //是否禁止播放音乐
        isJinzhiPlay : false, 
        //背景音乐ID
        audioId : null
    },
    

    /**
     * 发送请求
     * @param {*} url 请求接口路径
     * @param object params 参数
     * @param {*} method 请求方式 
     * @param {*} isSendToken 是否发送令牌(有的话) 默认true 
     */
    request (url, params = {}, method = 'POST', isSendToken = true, isRun2Exit = false) {
        let self = this;
        return new Promise((resolve, reject) => {
            let nonce = Math.random().toString(36).substr(2);
            let timestamp = parseInt(Date.now()/1000)
            let xhr = new XMLHttpRequest();

            if(isSendToken === true) {
                let token = self.userInfo.access_token;
                if(token) {
                    params['access_token'] = token;
                }
            }

            xhr.onreadystatechange = function (e) {
                if (xhr.readyState == 4 && xhr.status >= 200) {
                    let response = JSON.parse(xhr.responseText);
                    //判断是否为令牌过期
                    let checkResultCode = check_result(response,{
                        isRun2Exit : isRun2Exit
                    });
                    //令牌过期
                    if(checkResultCode == -1) {
                        wx.showLoading({title : '访问凭证刷新中,请耐心等待'});
                        self.request('user/refresh_access.html',{
                            access_token : self.userInfo.access_token,
                            refresh_token : self.userInfo.refresh_token
                        },'POST',true,true).then(res => {
                            //刷新成功
                            if(res.code == 1) {
                                self.userInfo.access_token = res.data.token;
                                //重新发起最后上一次的请求
                                if(params['access_token']) params['access_token'] = res.data.token;
                                self.request(url, params, method, isSendToken, isRun2Exit).then(res => {
                                    resolve(res);
                                }).catch(res => {
                                    reject(res);
                                })
                            }
                        })
                    } else if(checkResultCode == 1) {
                        resolve(response);
                    } else {
                        wx.removeStorageSync('userInfo')
                        wx.removeStorageSync('userInfoStorageDate')
                        reject();
                    }
                } else if(xhr.status == 400){
                    reject(response);
                }
            };
           
            xhr.open(method, this.hostConfig.host + url, true);
            xhr.timeout = 30000;
            //设置请求头
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
            xhr.setRequestHeader('appkey',this.hostConfig.appkey)
            xhr.setRequestHeader('nonce',nonce)
            xhr.setRequestHeader('timestamp',timestamp)
            xhr.setRequestHeader('sign',getSign(params, nonce, timestamp))
            let paramsStr = [];
            for(var i in params) {
                paramsStr.push((i + "=" + params[i]));
            }
            xhr.send(paramsStr.join('&'));
        });
    },
    // 转码
    encodeRFC5987ValueChars : encodeRFC5987ValueChars


};

module.exports = common;

//检测返回结果令牌是否过期
//return 0:失败 -1:过期  1:正常
function check_result(res, requestInfo) {
    let rest = res;
    if(rest.code == 0) {
        if(rest.data.errorCode != null) {
            switch(rest.data.errorCode) {
                case -1 :  case -3: case -4002 : 
                        wx.hideLoading();
                        let content,confirmColor = '#3cc51f';
                        if(rest.data.errorCode == -1) {
                            content = '凭证过期,请重新授权,请重新打开小程序';
                        } else {
                            content = '你的账号在其他地方被登陆了,请重新打开小程序';
                            confirmColor = 'red';
                        }
                        //重新登陆
                        wx.showModal({
                            title : '提示',
                            content : content,
                            confirmColor : confirmColor,
                            showCancel : false,
                            confirmText : '确定'
                        })
                    break;
                case -4001 : case -2 :
                        if(requestInfo.isRun2Exit === true && rest.data.errorCode == -2) {
                            wx.hideLoading();
                            //重新登陆
                            wx.showModal({
                                title : '提示',
                                content : '你的账号在其他地方被登陆了,请重新打开小程序',
                                confirmColor : 'red',
                                showCancel : false,
                                confirmText : '确定'
                            })
                        } else {
                            return -1;
                        }
                    break;
            }
        } else {
            return 0;
        }
    } else {
        return 1;
    }
    return 0;
}   



// 获取签名
function getSign(params, nonce, timestamp) {
    var arr = [];
    for (var i in params) {
        arr.push((i + "=" + (encodeRFC5987ValueChars(params[i]))));
    }
    return paramsStrSort(arr.join(("&")), nonce, timestamp);
}

function paramsStrSort(paramsStr, nonce, timestamp) {
    paramsStr = paramsStr != '' ? paramsStr += '&' : '' ;
    var url = paramsStr + "appkey=" + common.hostConfig.appkey + "&nonce=" + nonce + "&timestamp=" + timestamp;
    var urlStr = url.split("&").sort().join("&");
    var newUrl = urlStr + '&appsecret=' + common.hostConfig.appsecret;
    return md5.hexMD5(newUrl);
}

// 特殊字符串处理-为了兼容php上的url编码
function encodeRFC5987ValueChars (str) {
    return encodeURIComponent(str).
        // 注意，仅管 RFC3986 保留 "!"，但 RFC5987 并没有
        // 所以我们并不需要过滤它
        replace(/['()]/g, escape). // i.e., %27 %28 %29
        replace(/\*/g, '%2A').
            // 下面的并不是 RFC5987 中 URI 编码必须的
            // 所以对于 |`^ 这3个字符我们可以稍稍提高一点可读性
            replace(/%(?:7C|60|5E)/g, unescape);
}