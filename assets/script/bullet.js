// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.One = this.node.parent.getComponent('One');
        this.viewSize = cc.view.getVisibleSize();
    },

    start () {

    },

    init (config, bullet) {
        this.config = config;
        this.bullet = bullet;
    },

    update(dt) {
        if(this.node.y > (this.viewSize.height / 2)) {
            this.One.bulletPool.put(this.bullet);
        } else {
            //判断子弹类型
            if(this.config.bulletType == 1) {
                //三条路劲子弹
                switch(this.config.position) {
                    case 'left' : 
                            this.node.setPosition((this.node.x + 3), (this.node.y + 10));
                        break;
                    case 'center' : 
                        this.node.setPosition((this.node.x), (this.node.y + 10));
                        break;
                    case 'right' : 
                        this.node.setPosition((this.node.x-3), (this.node.y + 10));
                        break;
                }
            } else {
                //普通子弹
                this.node.setPosition(this.node.x, (this.node.y + 10));
            }
        }
    },

    onCollisionEnter() {
        //回收子弹
        this.One.bulletPool.put(this.bullet);
    }
    // update (dt) {},
});
