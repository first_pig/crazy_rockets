// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad () {
        this.height = (cc.view.getVisibleSize()).height / 2;
        this.isXh = false;
    },
    
    update () {
        //判断是否超过屏幕,超过则销毁当前节点
        if(this.isXh === false && ((!this.node.children[0]) || this.node.children[0].y <= '-' + this.height)) {
            this.isXh = true;
            this.node.destroy();
        }
    },
});
