const Cm = require('common');

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onCollisionEnter(e) {
        let OneNode = this.node.parent;
        if(e.node.name == "airdrop" || e.node.name == "airdropProp" || OneNode.getComponent('One').isDead === true) return false;
        let branch =  OneNode.getComponent('One').branch;
        //大于则更新
        if(branch > Cm.maxFenShu) {
            Cm.maxFenShu = OneNode.getComponent('One').branch;
        }
        
        OneNode.getComponent('One').isDead = true;
        OneNode.getComponent('One').sendTipsNode.getChildByName('tipsText').active = false;
        OneNode.getComponent('One').sendTipsNode.getChildByName('jifen').active = true;
        OneNode.getComponent('One').sendTipsNode.getChildByName('fuhuo').active = true;
        
        
        var anim = this.getComponent(cc.Animation);
        anim.play('feijiBlast');
        anim.on('finished',() => {
            //加载页面
            wx.showLoading({
                title : '分数上传中...',
                mask : true,
            })

            let uploadFractionFun = () => {
                Cm.request('user/upload_fraction',{
                    fraction : OneNode.getComponent('One').branch
                }).then(() => {
                    wx.hideLoading();
                }).catch(res => {
                    wx.showModal({
                        title : '提示',
                        content : '分数上传失败',
                        confirmText : '重试',
                        success (r) {
                            if(r.confirm) {
                                uploadFractionFun();
                            }
                        }
                    }) 
                });
            };
            uploadFractionFun();
            
            //删除该事件的监听
            anim.off('finished');
            this.node.parent.getComponent('One').endGame();
        })
        this.getComponent(cc.AudioSource).play();
    },

});
