// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        num : {
            type: cc.Integer,
            get : function() {
                return this._num;
            },
            set: function (value) {
                this._num = value;
                this.node.children[0].getComponent(cc.Label).string  = value
            }
        },
        label: {
            default: null,
            type: cc.Node
        },
        //块声效
        blockBurstSound : {
            default: null,
            type: cc.AudioClip
        },
        //块1的图片资源
        block1Bg : {
            default: null,
            type: cc.SpriteFrame
        },
        //块2的图片资源
        block2Bg : {
            default: null,
            type: cc.SpriteFrame
        }
    },

    onLoad () {
        //开启碰撞
        this.One = this.node.parent.parent.parent.getComponent('One');
        this.viewSize = cc.view.getVisibleSize();
        this.widgetNode = this.node.getComponent(cc.Widget);
        this.znum = 0;
    },

    update(dt) {
        if(this.node.y < -(this.viewSize.height / 2)) {
            this.node.cleanup();
            this.One.blockPool.put(this.block);
        } else {
            // this.node.setPosition(this.node.x, (this.node.y - 3));
            // this.node.setPosition(this.node.x, (this.node.y - (this.speed * dt)));
            this.node.y = (this.node.y - (this.speed * dt));
        }
    },


    onCollisionEnter (e) {
        if(e.node.name == 'feiji') return false;
        this.node.color = new cc.Color(218, 131, 231);
        if(this.num == 1 || this.num < 0) {
            //回收块
            //声音
            var audioID = cc.audioEngine.play(this.blockBurstSound, false, 1);
            cc.audioEngine.setVolume(audioID, 0.3);
            this.One.blockPool.put(this.block);
        } else {
            this.num -= 1;
        }
        this.scheduleOnce(() => {
            this.node.color = new cc.Color(255, 255, 255);
        },0.1)
        this.One.increaseBranch(1)
    },

    init (w,h,n,block,type,speed) {
        this.node.setContentSize(w,h);
        this.num = n;
        this.block = block;
        this.speed = speed;
        if(type == 1) {
            this.node.getComponent(cc.Sprite).spriteFrame = this.block1Bg;
        } else {
            this.node.getComponent(cc.Sprite).spriteFrame = this.block2Bg;
        }
    },
    
});
