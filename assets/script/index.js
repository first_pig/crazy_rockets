const Cm = require('common');

cc.Class({
    extends: cc.Component,

    properties: {
        //排行榜 预制体
        rankingPrefab : cc.Prefab,
        //背景音乐
        bgAudio : null
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

        //背景音乐
        if(Cm.bgAudioConfig.audioId == null) {
            Cm.bgAudioConfig.audioId = cc.audioEngine.play(this.bgAudio,true);
            cc.audioEngine.setVolume( Cm.bgAudioConfig.audioId, 0.5);
        } else {
            cc.audioEngine.resume(Cm.bgAudioConfig.audioId);
        }

        // return false;
        //加载页面
        wx.showLoading({
            title : '初始化中...',
            mask : true,
        })

        //适配
        var canvas = this.getComponent(cc.Canvas);
        var designResolution = canvas.designResolution
        var viewSize = cc.view.getFrameSize()
        this.viewSize = cc.view.getFrameSize();
        if (viewSize.width/viewSize.height > designResolution.width/designResolution.height) {
            canvas.fitHeight = true
        } else{
            canvas.fitWidth = true
        }
    },

    onEnable () {
        // return false;
        //监听点击开始事件
        this.node.getChildByName('go').on('touchstart',(e) => {
            this.node.getChildByName('go').getComponent(cc.AudioSource).play();
        })
        this.node.getChildByName('go').on('touchend',(e) => {
            cc.audioEngine.pause(Cm.bgAudioConfig.audioId);
            cc.director.loadScene('One')
        })

        //判断是否登陆过
        let userInfo = wx.getStorageSync('userInfo');
        if(userInfo.id) {
            wx.hideLoading();
            //判断超时时间是否超过10天
            let userInfoStorageDate = wx.getStorageSync('userInfoStorageDate'),
            maxTime = (userInfoStorageDate+864000);
            if(maxTime > parseInt(((new Date()).getTime() / 1000))) {
                this.node.getChildByName('loginTip').active = false;
                if(!Cm.userInfo.id) {
                    Cm.userInfo = userInfo;
                }  
                this.getFunshu();
                this.getPaihangbang();
            } else {
                this.loginInit();
            }
        } else {
            this.loginInit();
        }
    },

    //登陆
    loginInit () {
        let self = this;
        //判断是否授权过
        wx.getSetting({
            success (res) {
                wx.hideLoading();
                if(!res.authSetting['scope.userInfo']) {
                    //创建微信授权登陆按钮
                    const button = wx.createUserInfoButton({
                        type: 'text',
                        text : ' ',
                        style: {
                            left: 0,
                            top: 0,
                            width: self.viewSize.width,
                            height: self.viewSize.height
                        }
                    });
                    button.onTap(() => {
                        self.loginLc();
                        button.destroy();
                    })
                } else {
                    self.loginLc();
                }
            }
        })
        
    },


    //登陆逻辑
    loginLc () {
        //加载页面
        wx.showLoading({
            title : '登陆中...',
            mask : true,
        })
        let self = this;
        wx.login({
            success (loginRes) {
                wx.getUserInfo({
                    success (userInfo) {
                        Cm.request('user/login',{
                            port_type : 'wxxcx',
                            oauth_type : 'auth',
                            code : loginRes.code,
                            encryptedData : Cm.encodeRFC5987ValueChars(userInfo.encryptedData),
                            iv : Cm.encodeRFC5987ValueChars(userInfo.iv),
                        }).then(res => {
                            self.node.getChildByName('loginTip').active = false;
                            wx.hideLoading();
                            if(res.code == 0) {
                                wx.showModal({
                                    title : '提示',
                                    content : res.msg,
                                    showCancel : false
                                })
                            } else {
                                Cm.userInfo = res.data;
                                wx.setStorageSync('userInfo',res.data)
                                //写入用户信息的存入时间
                                wx.setStorageSync('userInfoStorageDate',parseInt(((new Date()).getTime() / 1000)))
                                self.getFunshu();
                                self.getPaihangbang();
                            }
                        }).catch(res => {
                            if(!res) {
                                wx.showModal({
                                    title : '提示',
                                    content : '登陆失败',
                                    showCancel : true,
                                    confirmText : '重新登陆',
                                    success (r) {
                                        if(r.confirm) {
                                            self.loginLc();
                                        }
                                    }
                                }) 
                            }
                        });
                    } 
                })

            }
        })
    },

    
    //获得分数
    getFunshu () {
        Cm.request('user/get_jifen').then(res => {
            wx.hideLoading();
            let jifen = res.data;
            Cm.maxFenShu = jifen;
            cc.find("myFenShu/num", this.node).getComponent(cc.Label).string = jifen;
        })
    },


    //获得排行榜信息
    getPaihangbang () {
        this.node.getChildByName('loading').active = true;
        Cm.request('Ranking/index',{}).then(res => {
            let contentNode = cc.find("rankingBg/view/content", this.node);
            let mapNum = 0;
            for(var i in res.data) {
                let prefab = cc.instantiate(this.rankingPrefab),
                data = res.data[i];
                //调整参数
                prefab.getChildByName('ranking').getComponent(cc.Label).string = mapNum + 1;
                //加载头像
                if(data.userInfo.avatar) {
                    var url = 'https://tper.jungongfang.cn/CrossOrigin.php?url=' + (data.userInfo.avatar);
                    httpRequest({
                        url: url,
                        async: true,
                        responseType: 'arraybuffer',
                        callback: function (data) {
                            //把请求回来的数据解析成图片的过程，图片的使用一定要在onload之后，如果不在onload之后，可能图片数据还没有解析出来，下面的代码就调用了，会报错。
                            var blob = new Uint8Array(data.response);
                            var img = new Image();
                            img.src = "data:image/png;base64," + Base64.encode(blob);
                            img.onload = function () {
                                var texture = new cc.Texture2D();
                                texture.generateMipmaps = false;
                                texture.initWithElement(img);
                                texture.handleLoadedTexture();
                                prefab.getChildByName('userAvatar').getChildByName('userAvatar').getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(texture);
                            }
                        }.bind(this)
                    });
                }
                //昵称
                prefab.getChildByName('userName').getComponent(cc.Label).string = data.userInfo.nickname;
                //积分
                prefab.getChildByName('userFenshu').getComponent(cc.Label).string = data.fraction;
                contentNode.addChild(prefab)
                prefab.getComponent(cc.Widget).isAlignTop = true;
                prefab.getComponent(cc.Widget).top = 180 * mapNum;
                prefab.getComponent(cc.Widget).updateAlignment();
                prefab.getChildByName('ranking').getComponent(cc.Widget).updateAlignment();
                mapNum++;
            }
            //调整包裹层高度
            if(mapNum >= 4) {
                contentNode.isAlignBottom = true;
                contentNode.bottom = -300;
            }
            this.node.getChildByName('loading').getComponent(cc.Widget).updateAlignment();
            this.node.getChildByName('loading').active = false;
        }).catch(res => {
            let self = this;
            wx.showModal({
                title : '提示',
                content : '排行榜获取失败',
                confirmText : '重试',
                success (r) {
                    if(r.confirm) {
                        self.getPaihangbang();
                    }
                }
            }) 
        });
    }
});


var httpRequest = function (params) {
    cc.log('发送请求', params.url);

    var urlStr = params.url;
    var methodStr = params.method || 'GET';
    var async = false;
    if (!!params.async) {
        async = true;
    }

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 400)) {
            if (!!params.callback) {
                cc.log('收到数据', xhr);
                params.callback(xhr);
            }
        }
    };

    if (!!params.responseType) {
        xhr.responseType = params.responseType;
    }
    xhr.open(methodStr, urlStr, async);
    xhr.send();
};

//base64
var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    // public method for encoding
    encode: function encode(input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        while (i < input.length) {
            chr1 = input[i++];
            chr2 = input[i++];
            chr3 = input[i++];

            enc1 = chr1 >> 2;
            enc2 = (chr1 & 3) << 4 | chr2 >> 4;
            enc3 = (chr2 & 15) << 2 | chr3 >> 6;
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output + Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) + Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);
        }
        return output;
    }
};