// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        //障碍块 预制资源
        blockPrefab : cc.Prefab,
        //子弹 预制资源
        bulletPrefab : cc.Prefab,
        //飞机
        feijiNode : cc.Node,
        //子弹发送音效
        bulletSendAudio: {
            default: null,
            type: cc.AudioClip
        },
        airdropNode : {
            default : null,
            type : cc.Sprite,
            displayName : '子弹空投节点'
        },
        airdropPropNode : {
            default : null,
            type : cc.Sprite,
            displayName : '道具空投节点'
        },
        sendTipsNode : {
            default : null,
            type : cc.Node,
            displayName : '结束界面节点'
        },
        bulletType : {
            default : 0,
            displayName : '初始子弹类型',
            tooltip : '0:单发 1:三连发'
        },
        branch : {
            default : 0,
            displayName : '初始积分'
        },
        blockMoveSpeed : {
            default : 3,
            displayName : '障碍的移动速度'
        },
        blockGenerateSpeed : {
            default : 3,
            displayName : '障碍的生成速度'
        },
        blockPromoteNum : {
            default : 100,
            displayName : '关卡提升倍数'
        },
        initBombNum : {
            default : 10,
            displayName : '初始炸弹数量'
        },
        fuhuoNum : {
            default : 3,
            type : cc.Integer,
            displayName : '免广告复活次数'
        },
        kanguanggaoFhuoNum : {
            default : 5,
            type : cc.Integer,
            displayName : '看广告复活次数'
        }
    },


    onLoad (e) {
        //开启碰撞
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        // manager.enabledDebugDraw = true;
        // manager.enabledDrawBoundingBox = true;

        //飞机层级
        this.feijiNode.zIndex = 5;
        //分数层级
        this.node.getChildByName('jifen').zIndex = 10;
        //暂停按钮
        this.node.getChildByName('stopBut').zIndex = 10;
        //炸弹道具布局位置
        this.node.getChildByName('bomb').zIndex = 10;
        //道具数量
        this.node.getChildByName('bomb').getChildByName('num').getComponent(cc.Label).string = this.initBombNum;
        //对提示节点层级调整
        this.sendTipsNode.zIndex = 11;
        //控制初始化
        this.controlInit();
        
        //对象池初始化
        this.blockInit(20);
        this.bulletInit(30);

        //适配
        var canvas = this.getComponent(cc.Canvas);
        var designResolution = canvas.designResolution
        var viewSize = cc.view.getFrameSize()
        if (viewSize.width/viewSize.height > designResolution.width/designResolution.height) {
            canvas.fitHeight = true
        } else{
            canvas.fitWidth = true
        }

        
    },

    start () {
        //开始生成
        this.blockStart();
        this.bulletStart();
        this.airdropStart();
    },

    onEnable () {
        this.resumeGame();
    },

    //增加积分
    increaseBranch (num) {
        var n = this.branch += num;
        this.node.getChildByName('jifen').getComponent(cc.Label).string  = n;
        return n;
    },

    //停止
    stopGame () {
        cc.director.pause();
    },

    //结束
    endGame () {
        cc.director.pause();
        this.sendTipsNode.getChildByName('jifen').getComponent(cc.Label).string = this.branch;
        this.sendTipsNode.getChildByName('fuhuo').getChildByName('Label').getComponent(cc.Label).string = '复活 x ' + parseInt(this.fuhuoNum + this.kanguanggaoFhuoNum);
        this.sendTipsNode.opacity = 255;
    },

    //恢复
    resumeGame () {
        cc.director.resume()
        this.sendTipsNode.opacity = 0;
    },

    //飞机控制初始化
    controlInit () {
        
        let width = ((this.node.width-this.feijiNode.width)/2),
        height = ((this.node.height-this.feijiNode.height)/2),
        //上次滑动信息
        lastSildeXy = null,
        x = 0,
        y = 0,
        addNum = 1;

        //点击复活
        this.sendTipsNode.getChildByName('fuhuo').on('touchstart',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 0) return false; 
            this.sendTipsNode.getChildByName('fuhuo').getComponent(cc.AudioSource).play();
        })
        this.sendTipsNode.getChildByName('fuhuo').on('touchend',(e) => {
            e.stopPropagation();
            lastSildeXy = null;
            if(this.sendTipsNode.opacity == 0) return false; 
            //这里要判断是否进入广告页面的,等上线后改
            let fuhuoNum = parseInt(this.fuhuoNum + this.kanguanggaoFhuoNum);
            if(fuhuoNum > 0) {
                this.isDead  = false;
                //将飞机的动画改为第一帧
                this.feijiNode.getComponent(cc.Animation).setCurrentTime(0,'feijiBlast');
                //清空所有障碍
                this.node.getChildByName('blockBg').removeAllChildren();
                if(this.fuhuoNum > 0) {
                    this.fuhuoNum--;
                } else if(this.kanguanggaoFhuoNum > 0) {
                    this.kanguanggaoFhuoNum--;
                }
                this.resumeGame();
            }
        })

        //重来
        this.sendTipsNode.getChildByName('chonglai').on('touchstart',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 0) return false;
            this.sendTipsNode.getChildByName('chonglai').getComponent(cc.AudioSource).play();
        })
        this.sendTipsNode.getChildByName('chonglai').on('touchend',(e) => {
            e.stopPropagation();
            lastSildeXy = null;
            if(this.sendTipsNode.opacity == 0) return false;
            this.node.targetOff();
            cc.director.loadScene('test')
        })

        //返回
        this.sendTipsNode.getChildByName('fanhui').on('touchstart',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 0) return false;
            this.sendTipsNode.getChildByName('fanhui').getComponent(cc.AudioSource).play()
        })
        this.sendTipsNode.getChildByName('fanhui').on('touchend',(e) => {
            e.stopPropagation();
            lastSildeXy = null;
            if(this.sendTipsNode.opacity == 0) return false;
            cc.director.loadScene('index')
        })

        //使用炸弹
        this.node.getChildByName('bomb').on('touchstart',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 255) return false;
            this.node.getChildByName('bomb').getComponent(cc.AudioSource).play();
        })
        this.node.getChildByName('bomb').on('touchend',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 255) return false;
            if(this.initBombNum > 0) {
                this.initBombNum--;
                this.node.getChildByName('blockBg').removeAllChildren();
                this.node.getChildByName('bomb').getChildByName('num').getComponent(cc.Label).string = this.initBombNum;
            } 
        })

        //暂停按钮点击监听
        this.node.getChildByName('stopBut').on('touchstart',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 255) return false;
            this.node.getChildByName('stopBut').getComponent(cc.AudioSource).play();
        })
        this.node.getChildByName('stopBut').on('touchend',(e) => {
            e.stopPropagation();
            if(this.sendTipsNode.opacity == 255) return false;
            this.sendTipsNode.getChildByName('tipsText').active = true;
            this.sendTipsNode.getChildByName('jifen').active = false;
            this.sendTipsNode.getChildByName('fuhuo').active = false;
            this.sendTipsNode.opacity = 255;
            this.stopGame();
        })


        this.node.on('touchstart', (e) => {
            lastSildeXy = null;
            if(this.sendTipsNode.opacity == 255 && this.isDead === true) return false;
            this.resumeGame();
        })

        this.node.on('touchend', () => {
            lastSildeXy = null;
        })

        this.node.on('touchcancel',() => {
            lastSildeXy = null;
        })

        this.node.on('touchmove', (e) => {
            if(e.touch._id != 0 || this.isDead == true) return;
            if(this.sendTipsNode.opacity == 255) return false;
            var xy = this.node.convertToNodeSpaceAR(e.getLocation());
            if(lastSildeXy != null && xy.x > lastSildeXy.x) {
                addNum = xy.x - lastSildeXy.x;
                if((this.feijiNode.x+addNum ) <= width) {
                    x = this.feijiNode.x + addNum ;
                    this.feijiNode.setPosition(x,this.feijiNode.y);
                }
            }
            
            if(lastSildeXy != null && xy.x < lastSildeXy.x) {
                addNum = lastSildeXy.x - xy.x;
                if((this.feijiNode.x-addNum ) >= parseInt('-'+width)) {
                    x = this.feijiNode.x - addNum ;
                    this.feijiNode.setPosition(x,this.feijiNode.y);
                }
            }
            
            if(lastSildeXy != null && xy.y > lastSildeXy.y) {
                addNum = xy.y - lastSildeXy.y;
                if((this.feijiNode.y+addNum ) <= height) {
                    y = this.feijiNode.y + addNum ;
                    this.feijiNode.setPosition(this.feijiNode.x,y);
                }
            }

            if(lastSildeXy != null && xy.y < lastSildeXy.y) {
                addNum = lastSildeXy.y - xy.y;
                if((this.feijiNode.y-addNum ) >= parseInt('-'+height)) {
                    y = this.feijiNode.y - addNum ;
                    this.feijiNode.setPosition(this.feijiNode.x,y);
                }
            }
                
            lastSildeXy = xy;
        })

    },

    //空投开始
    airdropStart () {
        let width = this.node.width,
        height = this.node.height;

        //子弹空投
        let airdropLogic = () => {
            //初始化空投位置
            //计算X轴
            let x = (Math.floor(Math.random()*((width - 313) / 2)+1));
            if(Math.round(Math.random())) {
                x = '-'+x;
            }
            this.airdropNode.getComponent('airdrop').init((type) => {
                if(type == 1) {
                    //碰到了 - 20秒的使用时间
                    this.bulletType = type
                    this.scheduleOnce(() => {
                        this.bulletType = 0;
                        this.scheduleOnce(airdropLogic,(Math.floor(Math.random()*10+1)))
                    },10)
                } else {
                    //过头了
                    this.scheduleOnce(airdropLogic,(Math.floor(Math.random()*10+1)))
                }
            }, 1);
            this.airdropNode.node.setPosition(x,(height / 2) - 10)
            this.airdropNode.node.zIndex = 5;
            this.airdropNode.node.active = true;
        };
        this.scheduleOnce(airdropLogic,15);
        // this.scheduleOnce(airdropLogic,1)

        //炸弹空投
        let bombLogic = () => {
            let x = (Math.floor(Math.random()*((width - 313) / 2)+1));
            if(Math.round(Math.random())) {
                x = '-'+x;
            }
            this.airdropPropNode.getComponent('airdrop').init((e) => {
                if(e == -1) {
                    this.initBombNum++;
                    this.node.getChildByName('bomb').getChildByName('num').getComponent(cc.Label).string = this.initBombNum;
                }
                this.scheduleOnce(bombLogic,(Math.floor(Math.random()*15+20)))
                // this.scheduleOnce(bombLogic,1)
            },-1)

            this.airdropPropNode.node.setPosition(x,(height / 2) - 10)
            this.airdropPropNode.node.zIndex = 5;
            this.airdropPropNode.node.active = true;
        };
        this.scheduleOnce(bombLogic,15);
        // this.scheduleOnce(bombLogic,1);
    },

    //子弹初始化
    bulletInit (count) {
        //创建block对象池
        this.bulletPool = new cc.NodePool();
        for(let i = 0; i < count; i++) {
            var enemy = cc.instantiate(this.bulletPrefab);
            this.bulletPool.put(enemy);
        }
    },

    //生成子弹
    bulletStart() {
        let width = this.node.width,
        height = this.node.height;

        this.schedule(() => {
            //判断子弹类型
            if(this.bulletType == 1) {
                for(var i = 0; i <= 2; i++) {
                    //取出对象
                    var bullet = null, position = null;
                    if(this.bulletPool.size() > 0) {
                        bullet = this.bulletPool.get();
                    } else {
                        bullet = cc.instantiate(this.bulletPrefab);
                    }
                    if(i == 0) {
                        position = 'left'
                    } else if(i == 1) {
                        position = 'center'
                    } else {
                        position = 'right'
                    }
                    bullet.getComponent('bullet').init({
                        bulletType : this.bulletType,
                        position : position
                    }, bullet);
                    bullet.setPosition(this.feijiNode.x,this.feijiNode.y);
                    bullet.zIndex = 1;
                    bullet.active = true;
                    this.node.addChild(bullet);
                    bullet.getComponent(cc.BoxCollider).size = {width:bullet.width, height:bullet.height}
                }
            } else {
                //取出对象
                var bullet = null;
                if(this.bulletPool.size() > 0) {
                    bullet = this.bulletPool.get();
                } else {
                    bullet = cc.instantiate(this.bulletPrefab);
                }
                bullet.getComponent('bullet').init({
                    bulletType : this.bulletType
                }, bullet);
                bullet.setPosition(this.feijiNode.x,this.feijiNode.y);
                bullet.zIndex = 1;
                bullet.active = true;
                this.node.addChild(bullet);
                bullet.getComponent(cc.BoxCollider).size = {width:bullet.width, height:bullet.height}
            }

            //声音
            var audioID = cc.audioEngine.play(this.bulletSendAudio, false, 1);
            cc.audioEngine.setVolume(audioID, 0.2);
        },0.1);
    },

    //block初始化
    blockInit (count) {
        //创建block对象池
        this.blockPool = new cc.NodePool();
        for(let i = 0; i < count; i++) {
            var enemy = cc.instantiate(this.blockPrefab);
            this.blockPool.put(enemy);
        }
    },

    // block 开始执行 - 生成障碍
    blockStart () {
        let width = this.node.width,
        height = this.node.height,
        blockBg = this.node.getChildByName('blockBg');
        
        let blockWidth1 = parseInt((width - 40) / 3), //3个块
        blockWidth2 = parseInt((width - 10) / 4); //4个块

        //障碍速度增加次数
        this.blockSpeedAddNum = 1;

        //逻辑
        let logic = () => {
            
            if(this.branch > (this.blockSpeedAddNum * this.blockPromoteNum) && this.blockSpeedAddNum < 6) {
                //根据积分逐渐加快速度
                if(this.blockMoveSpeed > 400) {
                    this.blockMoveSpeed += 20;
                }
                if(this.blockGenerateSpeed > 1.5) {
                    this.blockGenerateSpeed -= 0.20;
                }
                this.blockSpeedAddNum++;
            }

            //块的数量
            let blockNum = 3,
            //块宽度
            blockWidth = blockWidth1, 
            //间隔
            fx = (width - (blockWidth1 * 3)) / 4; 
            //判断是4个还是3个
            if(Math.round(Math.random())) {
            // if(true) {
                blockWidth = blockWidth2; 
                blockNum = 4;
                fx = (width - (blockWidth2 * 4)) / 6; 
            }

            //渲染优化,动态生成空节点.达到只渲染一次的效果
            let emptyNode = new cc.Node(this.guid());
            emptyNode.addComponent('rndEmptyNode')
            // console.log(this.node.getChildByName('blockBg').children)

            let minBlockKey = Math.floor(Math.random()*blockNum+1);
            for(var i = 0; i < blockNum; i++) {
               //取出对象
                let block = null, blockIife = Math.floor(Math.random()*60+1);
                if(this.blockPool.size() > 0) {
                    block = this.blockPool.get();
                } else {
                    block = cc.instantiate(this.blockPrefab);
                }

                //优化保证始终会有一个块是在5~10之间的
                if(i == minBlockKey) {
                    blockIife = this.rnd(15,20);
                }
                // blockIife = 999

                //坐标
                var x = ((width/2) - ((blockWidth+fx)*(i+1)));
                block.setPosition(x,(height/2)+100);
                
                //初始化
                block.getComponent('block').init(blockWidth, 100 , blockIife, block, (blockNum == 3 ? 1 : 2), this.blockMoveSpeed)
                emptyNode.addChild(block);
                block.getComponent(cc.BoxCollider).size = {width:block.width, height:block.height}
                block.getComponent(cc.BoxCollider).offset = {x:(block.width / 2),y:-(block.height / 2)}
                block.getChildByName('num').getComponent(cc.Widget).updateAlignment();
                block.active = true;
            }
            blockBg.addChild(emptyNode);

            //生成下一组
            this.scheduleOnce(function(){
                logic();
            },this.blockGenerateSpeed)
        };
        this.scheduleOnce(logic,1);
    },

    rnd(n, m){
        var random = Math.floor(Math.random()*(m-n+1)+n);
        return random;
    },

    //uuid
    guid() {
        function S4() {
           return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        }
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
    // update (dt) {},
});
