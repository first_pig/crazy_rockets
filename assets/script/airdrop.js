// Learn cc.Class:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://docs.cocos2d-x.org/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] https://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        bomb : {
            default : null,
            type : cc.SpriteFrame,
            displayName : '炸弹资源类型'
        }
    },

    onLoad () {
        this.viewSize = cc.view.getVisibleSize();
    },

    init (callback, bulletType) {
        this.callback = callback;
        this.bulletType = bulletType;

        //判断资源类型
        if(bulletType == -1) {
            //炸弹空投
            this.node.getComponent(cc.Sprite).spriteFrame = this.bomb;
        }
        
    },

    update (dt) {
        if(this.node.y < -(this.viewSize.height / 2)) {
            this.callback(0);
            this.node.active = false;
        } else {
            this.node.setPosition(this.node.x, (this.node.y - (200*dt)));
        }
    },
    
    onCollisionEnter () {
        this.callback(this.bulletType);
        this.node.active = false;
    },

    
});
